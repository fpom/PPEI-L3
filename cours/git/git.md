# `git`: commandes de base

- initialiser un projet
- ajouter des fichiers au contrôle de version
- enregistrer les changements
- inspecter l'historique des changements
- utiliser un serveur
- gérer les éditions concurrentes
- gérer les conflits

## Création d'un nouveau projet

- créer un répertoire et se placer dedans
- initialiser le dépôt `git`

```console
$ mkdir project
$ cd project
$ ls -la
total 8
drwxrwxr-x 2 franck franck 4096 Sep 18 17:56 .
drwxrwxr-x 4 franck franck 4096 Sep 18 17:56 ..
```

- en faire un dépôt `git`

```console
$ git init
Initialized empty Git repository in project/.git/
```

```console
$ ls -la
total 12
drwxrwxr-x 3 franck franck 4096 Sep 18 17:57 .
drwxrwxr-x 4 franck franck 4096 Sep 18 17:56 ..
drwxrwxr-x 7 franck franck 4096 Sep 18 17:57 .git
```

## Configurer `git`

- utiliser `git config --global ...` pour appliquer l'option à tous les dépôts
- il restera possible de configurer chaque dépôt différement en faisant comme ci-dessous
- la configuration d'un dépôt n'est pas versionnée, elle reste locale

```console
$ git config user.name "Franck Pommereau"
$ git config user.email "franck.pommereau@univ-evry.fr"
$ git config core.editor "kitty nvim"
```

> **Note:** j'utilise l'éditeur `nvim` qui fonctionne dans un terminal, c'est pourquoi je lance le terminal `kitty` (mon terminal habituel) en lui demandant de lancer `nvim`.
> Pour éditer un fichier `xxx`, `git` lancera donc `kitty nvim xxx` ce qui ouvrira `nvim` sur le fichier `xxx` dans un terminal `kitty`.
> Utilisez l'éditeur avec lequel vous êtes le plus à l'aise.

## Ajouter des fichiers

- créer un fichier
- contrôler l'état du dépôt

```console
$ edit README.md
# Sample project
EOF
```

> **Note:** la commande `edit` ci-dessus n'existe que dans ce cours, elle lit du texte jusqu'à `EOF` et l'ajoute à la fin du fichier passé en ligne de commande.
> En pratique on utilisera un éditeur plus convivial, mais moins adapté à des copier-coller dans un cours.

```console
$ cat README.md
# Sample project
$ git status
On branch main

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	README.md

nothing added to commit but untracked files present (use "git add" to track)
```

- ajouter le fichier au dépôt

```console
$ git add README.md
$ git status
On branch main

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   README.md
```

- enregistrer les changements

```console
$ git commit -m "first version"
[main (root-commit) c52f7f0] first version
 1 file changed, 1 insertion(+)
 create mode 100644 README.md
$ git status
On branch main
nothing to commit, working tree clean
```

## Tracer les changements

- modifier un fichier
- contrôler l'états du dépôt

```console
$ edit README.md

This is a sample project.
To be continued...
EOF
$ cat README.md
# Sample project

This is a sample project.
To be continued...
$ git status
On branch main
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git restore <file>..." to discard changes in working directory)
	modified:   README.md

no changes added to commit (use "git add" and/or "git commit -a")
```

- voir les changements

```console
$ git diff
diff --git a/README.md b/README.md
index a737df5..30f09a7 100644
--- a/README.md
+++ b/README.md
@@ -1 +1,4 @@
 # Sample project
+
+This is a sample project.
+To be continued...
```

- enregistrer les changements
- voir l'historique

```console
$ git commit -a -m "updated README.md"
[main 1c0a3bf] updated README.md
 1 file changed, 3 insertions(+)
$ git log
commit 1c0a3bfdb35f6a846e3b0971654b947518d442b4 (HEAD -> main)
Author: Franck Pommereau <franck.pommereau@univ-evry.fr>
Date:   Wed Sep 18 18:13:32 2024 +0200

    updated README.md

commit c52f7f0c02c7d10cf65b08eeb8266e44ef1e6cba
Author: Franck Pommereau <franck.pommereau@univ-evry.fr>
Date:   Wed Sep 18 18:05:02 2024 +0200

    first version
```

## Ajouter d'autres fichiers

```console
$ edit src/hello.c
#include <stdio.h>

int main (void) {
  printf("Hello world!\n");
}
EOF
$ edit Makefile
hello: src/hello.c
<TAB>gcc -c src/hello.c
<TAB>gcc -o hello *.o
EOF
```

> **Note:** notre commande `edit` interprète `<TAB>` comme une tabulation.

```console
$ git status
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	Makefile
	src/

nothing added to commit but untracked files present (use "git add" to track)
$ git add src/hello.c Makefile
$ git commit -m "added source code and Makefile"
[main 130530b] added source code and Makefile
 2 files changed, 8 insertions(+)
 create mode 100644 Makefile
 create mode 100644 src/hello.c
```

## Ignorer certains fichiers

- on a souvent des fichiers encombrants qui n'ont pas besoin d'être versionnés
  - par exemple les résultats de compilations
  - des fichiers de log
  - etc.

```console
$ make
gcc -c src/hello.c
gcc -o hello *.o
$ git status
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	hello
	hello.o

nothing added to commit but untracked files present (use "git add" to track)
```

- fichier `.gitignore`

```console
$ edit .gitignore
/hello
*.o
EOF
$ git status
On branch main
Untracked files:
  (use "git add <file>..." to include in what will be committed)
	.gitignore

nothing added to commit but untracked files present (use "git add" to track)
$ git add .gitignore
$ git commit -m "added .gitignore"
[main 35a7ed4] added .gitignore
 1 file changed, 2 insertions(+)
 create mode 100644 .gitignore
```

## Sauver le dépôt sur une forge

- créer un compte sur une [Codeberg](https://codeberg.org)
  - d'autres forges existent et fonctionnent de manière similaire
  - [GitHub](https://github.com) plus (trop) de fonctionnalités, code fermé, dépôts utilisés pour entraîner des IA en violation des licences d'utilisation
  - [GitLab](https://gitlab.com) version communautaire opensource
- créer un dépôt
- suivre les instructions pour envoyer notre code
  - avec une clef SSH, connexion sans mot de passe
  - sinon, le mot de passe est demandé

```console
$ git remote add origin git@codeberg.org:fpom/project.git
$ git push -u origin main
Enumerating objects: 14, done.
Counting objects: 100% (14/14), done.
Delta compression using up to 8 threads
Compressing objects: 100% (9/9), done.
Writing objects: 100% (14/14), 1.18 KiB | 1.18 MiB/s, done.
Total 14 (delta 1), reused 0 (delta 0), pack-reused 0
To codeberg.org:fpom/project.git
 * [new branch]      main -> main
Branch 'main' set up to track remote branch 'main' from 'origin'.
```

## Mettre à jour sur le serveur

- faire des commits locaux
- les pousser

```console
$ edit Makefile

clean:
<TAB>rm -f hello *.o
EOF
$ git commit -a -m "added make clean"
[main a082c10] added make clean
 1 file changed, 3 insertions(+)
$ git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 320 bytes | 320.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
To codeberg.org:fpom/project.git
   35a7ed4..a082c10  main -> main
```

## Cloner un projet depuis serveur

- récupérer le dépôt du serveur dans un autre répertoire
- on aura deux versions du dépôts du projet
  - `project` le dépôt primaire duquel on est parti, géré par _Franck Pommereau_
  - `project-bis` un dépôt secondaire pour simuler la collarboration avec _Alain Turing_

```console
$ cd ..
$ git clone git@codeberg.org:fpom/project.git project-bis
Cloning into 'project-bis'...
remote: Enumerating objects: 17, done.
remote: Counting objects: 100% (17/17), done.
remote: Compressing objects: 100% (12/12), done.
remote: Total 17 (delta 3), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (17/17), done.
Resolving deltas: 100% (3/3), done.
$ cd project-bis
$ ls -la
total 28
drwxrwxr-x 4 franck franck 4096 Sep 18 18:21 .
drwxrwxr-x 5 franck franck 4096 Sep 18 18:21 ..
drwxrwxr-x 8 franck franck 4096 Sep 18 18:21 .git
-rw-rw-r-- 1 franck franck   11 Sep 18 18:21 .gitignore
-rw-rw-r-- 1 franck franck   82 Sep 18 18:21 Makefile
-rw-rw-r-- 1 franck franck   63 Sep 18 18:21 README.md
drwxrwxr-x 2 franck franck 4096 Sep 18 18:21 src
```

- configurer le projet
- faire un changement
- l'enregistrer et le pousser

```console
$ git config user.name "Alain Turing"
$ git config user.email "franck.pommereau@ibisc.univ-evry.fr"
$ git config core.editor "kitty nvim"
$ edit README.md

Project updated from another repo.
EOF
$ git commit -a -m "update bis"
[main a33c7b4] update bis
 1 file changed, 2 insertions(+)
$ git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 346 bytes | 346.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
To codeberg.org:fpom/project.git
   a082c10..a33c7b4  main -> main
```

## Récupérer les mises à jour depuis le serveur

- retour du dépôt primaire pour y récupérer les changements d'Alain Turing

```console
$ cd ../project
$ git pull
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 1), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 326 bytes | 326.00 KiB/s, done.
From codeberg.org:fpom/project
   a082c10..a33c7b4  main       -> origin/main
Updating a082c10..a33c7b4
Fast-forward
 README.md | 2 ++
 1 file changed, 2 insertions(+)
$ cat README.md
# Sample project

This is a sample project.
To be continued...

Project updated from another repo.
```

## Éditions concurrentes et merge

- on ne peut pas `push` vers un serveur qui possède une version plus récente
  - il faut `pull` d'abord
  - et donc gérer la fusion de versions différentes
- d'abord on change depuis le dépôt primaire

```console
$ edit README.md
One more line...
EOF
$ git commit -a -m "update from main repo"
[main 19b0a94] update from main repo
 1 file changed, 1 insertion(+)
$ git push
Enumerating objects: 5, done.
Counting objects: 100% (5/5), done.
Delta compression using up to 8 threads
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 314 bytes | 314.00 KiB/s, done.
Total 3 (delta 2), reused 0 (delta 0), pack-reused 0
To codeberg.org:fpom/project.git
   a33c7b4..19b0a94  main -> main
```

- retour au dépôt secondaire

```console
$ cd ../project-bis
$ edit TODO.md
 * add documentation
EOF
$ git add TODO.md
$ git commit -m "update from secondary repo"
[main 25e2eef] update from secondary repo
 1 file changed, 1 insertion(+)
 create mode 100644 TODO.md
```

- on tente de le pousser

```console
$ git push
To codeberg.org:fpom/project.git
 ! [rejected]        main -> main (fetch first)
error: failed to push some refs to 'codeberg.org:fpom/project.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

- il y a deux branches séparées partant du précédent commit:
  - celle créée depuis le dépôt principal
  - celle créée depuis le dépôt secondaire
- il suffit de faire ce qui est dit dans le message

```console
$ git pull
remote: Enumerating objects: 5, done.
remote: Counting objects: 100% (5/5), done.
remote: Compressing objects: 100% (3/3), done.
remote: Total 3 (delta 2), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (3/3), 294 bytes | 294.00 KiB/s, done.
From codeberg.org:fpom/project
   a33c7b4..19b0a94  main       -> origin/main
hint: Waiting for your editor to close the file...
Merge made by the 'ort' strategy.
 README.md | 1 +
 1 file changed, 1 insertion(+)
```

> **Note:** sur la commande précédente, `git` a réussi à fusionner les deux branches automatiquement.
> Cette fusion est enregistrée dans un nouveau commit.
> `git` a donc lancé l'éditeur qu'on avait configuré pour nous permettre d'éditer le message de commit.

- on peut voir le log
- mais le graphe des commits est plus explicite
  - avec `gitk` localement
  - sur la forge

## Gérer les conflits

- quand les changements sont incompatibles
- `git` montre des versions à réconcilier manuellement

```console
$ edit README.md
One line from secondary repo...
EOF
$ git commit -a -m "from secondary"
[main 47fc667] from secondary
 1 file changed, 1 insertion(+)
$ git push
Enumerating objects: 12, done.
Counting objects: 100% (12/12), done.
Delta compression using up to 8 threads
Compressing objects: 100% (7/7), done.
Writing objects: 100% (8/8), 962 bytes | 962.00 KiB/s, done.
Total 8 (delta 3), reused 0 (delta 0), pack-reused 0
To codeberg.org:fpom/project.git
   19b0a94..47fc667  main -> main
```

- retour au dépôt principal

```console
$ cd ../project
$ edit README.md
One line from primary repo...
EOF
$ git commit -a -m "from primary"
[main 56da1f5] from primary
 1 file changed, 1 insertion(+)
$ git push
To codeberg.org:fpom/project.git
 ! [rejected]        main -> main (fetch first)
error: failed to push some refs to 'codeberg.org:fpom/project.git'
hint: Updates were rejected because the remote contains work that you do
hint: not have locally. This is usually caused by another repository pushing
hint: to the same ref. You may want to first integrate the remote changes
hint: (e.g., 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

```console
$ git pull
remote: Enumerating objects: 12, done.
remote: Counting objects: 100% (12/12), done.
remote: Compressing objects: 100% (7/7), done.
remote: Total 8 (delta 3), reused 0 (delta 0), pack-reused 0
Unpacking objects: 100% (8/8), 942 bytes | 942.00 KiB/s, done.
From codeberg.org:fpom/project
   19b0a94..47fc667  main       -> origin/main
Auto-merging README.md
CONFLICT (content): Merge conflict in README.md
Automatic merge failed; fix conflicts and then commit the result.
```

- cette fois le merge automatique a échoué
- en effet, les deux commit ajoutent une ligne à la fin du fichier
  - `git` ne peut pas savoir laquelle il faut mettre en premier
  - on pourrait même vouloir ne mettre qu'une seule ligne

```console
$ git status
On branch main
Your branch and 'origin/main' have diverged,
and have 1 and 3 different commits each, respectively.
  (use "git pull" to merge the remote branch into yours)

You have unmerged paths.
  (fix conflicts and run "git commit")
  (use "git merge --abort" to abort the merge)

Changes to be committed:
	new file:   TODO.md

Unmerged paths:
  (use "git add <file>..." to mark resolution)
	both modified:   README.md
$ cat README.md
# Sample project

This is a sample project.
To be continued...

Project updated from another repo.
One more line...
<<<<<<< HEAD
One line from primary repo...
||||||| 19b0a94
=======
One line from secondary repo...
>>>>>>> 47fc667d1dacbbed0d2a63f81ebbc98c62787893
```

- on a les deux modifications incompatibles listées à la suite l'un de l'autre
- le conflit est encadré par deux lignes `<<<<<<< ...` et `>>>>>>> ...`
- les deux changements sont séparés par la ligne `=======` qui peut être précédée d'une ligne `||||||| ...`
- pour résoudre le conflit, on édite le fichier pour le mettre dans l'état souhaité, en enlevant les lignes délimitant le conflit

```console
$ kitty nvim README.md
$ cat README.md
# Sample project

This is a sample project.
To be continued...

Project updated from another repo.
One more line...
One line from primary repo...
One line from secondary repo...
```

- on a choisi de garder les deux lignes, dans l'ordre _primary_ puis _secondary_
- on aurait pu aussi bien les fusionner en une seule
- reste à commit et push les changements

```console
$ git add README.md
$ git commit -m "solved conflict"
[main 63c2024] solved conflict
$ git push
Enumerating objects: 10, done.
Counting objects: 100% (10/10), done.
Delta compression using up to 8 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (6/6), 579 bytes | 579.00 KiB/s, done.
Total 6 (delta 4), reused 0 (delta 0), pack-reused 0
To codeberg.org:fpom/project.git
   47fc667..63c2024  main -> main
```

## Requêtes de `pull`/`merge`

- sur les forges quand on veut contribuer à un projet
  - les responsables ne donnent généralement pas le droit de `commit` directement
  - on `fork` le projet ➡️ crée un dépôt identique dont on est propriétaire
  - on l'utilise comme un dépôt qu'on aurait créé soi même
  - quand on veut proposer les changements, on initie une requête de `pull` (_`pull` request_, parfois appelé _`merge` request_ sur certaines forges)
  - les responsables du dépôt d'origine examinent nos changements, éventuellement ouvrir un dialogue pour demander des modifications, puis accepter ou pas la requête
  - si elle est acceptée, la requête est `merge` dans le dépôt d'origine
